# gitea + 🐳

This is a template for deploying a Gitea instance with some custom settings.

## Notes

- The template is meant for personal usage, in case I forget the settings.
- The additional renderers don't work out of the box. You need to `exec` into the containers and install `asciidoctor`
- Should be set up with user `git` on the host system
- Some useful official guides I do want to take note:
  - [SSH passthrough](https://docs.gitea.io/en-us/install-with-docker/#ssh-container-passthrough)
  - [Additions to Gitea pages](https://docs.gitea.io/en-us/customizing-gitea/#other-additions-to-the-page)
  - [Serve static resources directly](https://docs.gitea.io/en-us/reverse-proxies/#two-nodes-and-two-domains)
  - [Signed commits](https://docs.gitea.io/en-us/signing/)
  - [Additional renderers](https://docs.gitea.io/en-us/external-renderers/)
  - [No robots](https://docs.gitea.io/en-us/search-engines-indexation/)
