# Pi-hole + 🐳

## Introduction

This repository holds docker template for [Pi-hole](https://docs.pi-hole.net/) with [Unbound](https://unbound.net) + [Stubby](https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+Daemon+-+Stubby) on top, running in my home network as both DNS server and DHCP server (using bridge network). [Wireguard](https://www.wireguard.com) is included for convenience.

The DHCP part is yanked from [@DerFetzer](https://discourse.pi-hole.net/t/dhcp-with-docker-compose-and-bridge-networking/17038), with some improvements. The Unbound and Wireguard parts are based on [Wirehole](https://github.com/IAmStoxe/wirehole). You can also check out [this gist](https://gist.github.com/Jiab77/1cdc2896f22791c4db492e87bbf609ff) and the [official Unbound guide](https://docs.pi-hole.net/guides/dns/unbound).

## Deployment

You should change the variables in `docker-compose.yml` file. If you want to change the default IP addresses of the internal network, remember to also replace the forward address in `unbound.conf`, and add the new IP range to "access-control" and "private-address" accordingly.

- Clone this repository
- Review the content
- `docker-compose up -d`
- Route DNS traffic from your router to the Pi-hole server
- Turn on Pi-hole's DHCP server and turn off DHCP function in your router
- Add more gravity lists. [firebog.net](https://firebog.net) has some good ones
- Profit

## Notes

- To check whether recursive DNS is working, you can inspect with `ngrep` or `tcpdump`:
```bash
# Check Unbound
ngrep -d br-pihole port 53 and host 10.2.0.200

# Check Stubby
ngrep -d br-pihole port 8053 and host 10.2.0.150
# tcpdump -vv -x -X -s 1500 -i br-pihole 'port 8053'

# Check FTLDNS
ngrep -d lo port 53
```
- To check DNSSEC validation, use `drill`:
```bash
# You should see 'Error: Bogus DNSSEC signature' with exit code 5
drill -DT sigfail.verteiltesysteme.net

# This command should run successfully
drill -DT sigok.verteiltesysteme.net
```
- To display the peer's QR code again, do `docker exec -it wireguard /app/show-peer [peer_name]`.
- Stubby isn't packaged for Alpine, so I use Voidlinux-musl for now.
- You can omit the hostname variables for **pi-hole** and **unbound** in `docker-compose.yml`. The pi-hole's one is for the displayed hostname (top right corner) in the web UI. The other one just makes queries logs easier to read.
- The default Unbound config use `so-rcvbuf: 1m` and `so-sndbuf: 1m`. This require a bigger values of `net.core.wmem_max` and `net.core.rmem_max` than the default ones in Alpine (212992). 1050000 is enough in this case.
- You can also run Docker Pi-hole as your DHCP server using host network or Macvlan network. Check the [official documentation](https://docs.pi-hole.net/docker/dhcp/).
- `DNSMASQ_LISTENING: all` is needed, because Pi-hole will listen on both the host network (for DNS requests), and the subnet `br-pihole`. This brings some security risks, so make sure to only run this inside your home network, and do not forward port 53 from your router to the Pi-hole server.
- dhcphelper needs `network_mode: host` because it has to be seen by clients on your local network. Though it only needs port 67.
- If you don't like `dhcphelper`, here is the diff for using ISC DHCP relay instead:

**Dockerfile:**

```Dockerfile
FROM debian:stable-slim
RUN apt update

RUN apt install -y isc-dhcp-relay
EXPOSE 67 67/udp
ENTRYPOINT ["dhcrelay", "-d"]
```

**docker-compose.yml:**

```diff
   dhcprelay:
     build: .
     restart: unless-stopped
     network_mode: "host"
-    command: ["-i", "eth0", "-b", "br-pihole", "-s", "10.2.0.100"]
+    command: ["-id", "eth0", "-iu", "br-pihole", "10.2.0.100"]
     cap_add:
       - NET_ADMIN
```

## License

MIT
