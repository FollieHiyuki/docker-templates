# You can always run `MIX_ENV=prod mix pleroma.instance gen` to have a base config
use Mix.Config

config :pleroma, Pleroma.Web.Endpoint,
  url: [host: System.get_env("DOMAIN", "localhost"), scheme: "https", port: 443],
  http: [ip: {0, 0, 0, 0}, port: 4000],
  secret_key_base: "<use 'openssl rand -base64 48' to generate a key>",
  signing_salt: ""

config :pleroma, :gopher,
  enabled: true,
  ip: {0, 0, 0, 0},
  port: 70

config :pleroma, :media_proxy,
  enabled: true,
  proxy_opts: [
	  redirect_on_failure: true
  ]
  #base_url: "https://cache.kokkoro.moe"

config :pleroma, :frontend_configurations,
  pleroma_fe: %{
    showInstanceSpecificPanel: true,
    theme: "nord-one",
    background: "/images/kokorow.png",
    logo: "/static/logo.png"
  }

config :pleroma, :instance,
  name: System.get_env("INSTANCE_NAME", "Pleroma"),
  email: System.get_env("ADMIN_EMAIL"),
  notify_email: System.get_env("NOTIFY_EMAIL"),
  limit: 5000,
  registrations_open: false,
  invites_enabled: true,
  account_approval_required: false,
  federating: true,
  healthcheck: true

config :pleroma, Pleroma.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("DB_USER", "pleroma"),
  password: System.fetch_env!("DB_PASS"),
  database: System.get_env("DB_NAME", "pleroma"),
  hostname: System.get_env("DB_HOST", "db"),
  pool_size: 10

config :web_push_encryption, :vapid_details,
  subject: "mailto:#{System.get_env("NOTIFY_EMAIL")}",
  public_key: "",
  private_key: ""

config :pleroma, :database, rum_enabled: false
config :pleroma, :instance, static_dir: "/var/lib/pleroma/static"
config :pleroma, Pleroma.Uploaders.Local, uploads: "/var/lib/pleroma/uploads"

config :pleroma, :http_security,
  enabled: true,
  sts: true,
  referrer_policy: "same-origin"

config :joken, default_signer: ""

config :pleroma, configurable_from_database: false

config :pleroma, Pleroma.Upload, filters: [Pleroma.Upload.Filter.Exiftool, Pleroma.Upload.Filter.AnonymizeFilename]
