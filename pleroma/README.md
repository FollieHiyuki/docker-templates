# Pleroma + 🐳

Docker template and configuration for Pleroma powering [kokkoro.moe](https://kokkoro.moe). This is a shorter version compared to [@sn0w's one](https://memleak.eu/sn0w/pleroma-docker), which means you have to do all the maintenance (updating, running mix tasks) by hands and it doesn't have any safe checks.

## Notes

- Deploy: run `build.sh` and `docker-compose up --remove-orphan -d`
- Update: `docker-compose pull` (update postgres image) and rebuild
- Exec into the container: `docker-compose exec web sh -c 'cd /pleroma && /bin/ash'`

## Postgres upgrades

Postgres upgrades are a slow process in docker (even more than usual) because we can't utilize `pg_upgrade` in any sensible way.<br/>
If you ever wish to upgrade postgres to a new major release for some reason, here's a list of things you'll need to do.

- Inform your users about the impending downtime
- Seriously this can take anywhere from a couple hours to a week depending on your instance
- Make sure you have enough free disk space or some network drive to dump to, we can't do in-place upgrades
- Stop pleroma (`docker-compose stop server`)
- Dump the current database into an SQL file (`docker-compose exec db pg_dumpall -U pleroma > /my/sql/location/pleroma.sql`)
- Remove the old containers (`docker-compose down`)
- Modify the postgres version in `docker-compose.yml` to your desired release
- Delete `data/db` or move it into some different place (might be handy if you want to abort/revert the migration)
- Start the new postgres container (`docker-compose up -d db`)
- Start the import (`docker-compose exec -T db psql -U pleroma < /my/sql/location/pleroma.sql`)
- Wait for a possibly ridculously long time
- Boot pleroma again (`docker-compose up -d`)
- Wait for service to stabilize while federation catches up
- Done!
