#!/bin/sh

cacheTag=""

echo "Generating a random tag..."

if command -v git >/dev/null; then
    resolveHash=$(git ls-remote https://git.pleroma.social/pleroma/pleroma.git | grep "/develop" | awk '{ print $1 }')
    if [ -n "$resolveHash" ]; then
        cacheTag="$resolveHash"
    fi
fi

if [ -z "$cacheTag" ] && command -v date; then
    cacheTag="$(date '+%s')"
fi

if [ -z "$cacheTag" ]; then
    echo "Can't get a random variable. Stopped."
    exit 1
fi

echo "Building Pleroma..."
docker-compose build \
    --build-arg __CACHE_TAG="$cacheTag" \
    web
