#!/bin/ash

set -e

echo "-- Adding crontab ..."
echo "0 0,12 * * * root python -c 'import random; import time; time.sleep(random.random() * 3600)' && certbot renew -q" | crontab -

echo "-- Starting nginx ..."
nginx -c /etc/nginx/nginx.conf
