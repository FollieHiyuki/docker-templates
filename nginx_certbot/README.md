# Nginx + Certbot + 🐳

## Introduction

This is a Docker template for nginx + certbot deployment, with stream support. I also include configurations for forwarding on ports 70 and 6443, and also for my personal website, as examples.

`http.d` directory contains configurations for normal http/https traffics. The other one, `stream.d` is for stream configurations.

## How to use

- Clone this repository
- Change the content to your need, also mount necessary additional volumes
- Do `docker-compose up -d`
- Generate LetsEncrypt certificate by `docker-compose exec nginx sh -c 'certbot --nginx certonly -d yourdomain.tld'`
- Restart the container
- Profit
