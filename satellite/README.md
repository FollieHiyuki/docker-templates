# Satellite + 🐳

## Introduction

This repository holds docker template for [Satellite](https://sr.ht/~gsthnz/satellite) (an easy to use Gemini server). Satellite already provides `Dockerfile`, so I only create `docker-compose.yml` for easier build + deployment.

## Deployment

You should change the domain name in `config.toml`, as well as Docker's volume paths, opened port (default is 1965).

The `Dockerfile` uses UID=1000, so assure your normal user has the same UID.

- Clone this repository
- Change directory into it and do `git submodule update --init`
- `cp config.toml satellite/config/ && chmod 700 satellite/config`
- `docker-compose up -d`
- Wait for Satellite to build
- Profit
