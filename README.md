# docker-templates

Docker templates for personal usage.

This repo soon will be only for reference. Look at [home-infra](https://git.disroot.org/FollieHiyuki/home-infra) instead.

## Prerequisites

```shell
# apk add docker-cli-compose docker-engine docker-openrc
```

## License

MIT
